/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 13_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once
const char GASOLINE_GALLON_EQUIVALENT = 'g';
const double GASOLINE_PER_JOULE = (1/1.213e8);


extern double fromGasolineToJoule( double gasoline );
extern double fromJouleToGasoline(double joule);
