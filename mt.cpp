/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file mt.cpp
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 13_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "mt.h"

double fromMegatonToJoule( double megaton ) {
   return megaton / MEGATON_PER_JOULE;
}

double fromJouleToMegaton( double joule ) {
   return joule * MEGATON_PER_JOULE;
}


