/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file f.cpp
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 13_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "f.h"

double fromFoeToJoule( double foe ){
   return foe / FOE_PER_JOULE;
}

double fromJouleToFoe(double joule){
   return joule * FOE_PER_JOULE;
}

