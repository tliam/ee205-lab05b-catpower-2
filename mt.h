/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file mt.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 13_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once
const char MEGATON = 'm';
const double MEGATON_PER_JOULE = (1/4.184e15);


extern double fromMegatonToJoule( double megaton );
extern double fromJouleToMegaton( double joule );

